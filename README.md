# desafios-DIO

Esse repositório contém listas de desafios da plataforma Digital Innovation One, onde contém exercícios com resolução em Java de diversos bootcamps. 

Esses exercícios são bases fundamentais para o entendimento e resoluções de lógica, onde são aplicados princípios básicos, intermediários e avançados 
para chegarmos à solução dos problemas.
